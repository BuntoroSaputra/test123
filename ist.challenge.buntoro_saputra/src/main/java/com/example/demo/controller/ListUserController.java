package com.example.demo.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.User;
import com.example.demo.repository.UserRepository;


@CrossOrigin(origins = "http://localhost:8081")
@RestController
@RequestMapping("/api")

public class ListUserController {

	@Autowired
	private UserRepository userRepository;
	
	@GetMapping("/users")
	public List<User> getAllUser(){
		return userRepository.findAll();
	}		
	
	@PutMapping("/updateuser")
	public ResponseEntity<String> updateUser(@RequestBody User userEdit){
		User user = userRepository.findById(userEdit.getId()).get();
		
		User otherUser = userRepository.findByUsername(userEdit.getUsername());
		if (otherUser != null) {
			// validasi Username sudah terpakai 
			if (user.getId() != otherUser.getId()) {
				return new ResponseEntity<>(
				          "Username sudah terpakai", 
				          HttpStatus.CONFLICT);
			}
		}
		
		
		// Password tidak boleh sama dengan password sebelumnya
		if (user.getPassword().equals(userEdit.getPassword())) {
			return new ResponseEntity<>(
			          "Password tidak boleh sama dengan password sebelumnya", 
			          HttpStatus.BAD_REQUEST);
		}
		
		user.setUsername(userEdit.getUsername());
		user.setPassword(userEdit.getPassword());
		
		User updatedUser = userRepository.save(user);
		return new ResponseEntity<>(
		          "", 
		          HttpStatus.CREATED); 
	}
	
	
}
