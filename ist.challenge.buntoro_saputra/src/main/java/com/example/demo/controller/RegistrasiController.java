package com.example.demo.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.User;
import com.example.demo.repository.UserRepository;

@CrossOrigin(origins = "http://localhost:8081")
@RestController
@RequestMapping("/api")
public class RegistrasiController {

	@Autowired
	UserRepository userRepository;

	// save user
	@PostMapping("/registrasi")
	public ResponseEntity<String> createTutorial(@RequestBody User user) {

		if (userRepository.findByUsername(user.getUsername()) != null) { 
			// data user sudah ada
			return new ResponseEntity<>(
					"UserName sudah terpakai", 
					HttpStatus.CONFLICT);
		}
		else {
			// simpan data
			userRepository.save(user);

			return new ResponseEntity<>(
					"", 
					HttpStatus.CREATED);

		}

	}
	
}
