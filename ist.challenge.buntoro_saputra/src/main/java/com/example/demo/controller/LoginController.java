package com.example.demo.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.User;
import com.example.demo.repository.UserRepository;


@CrossOrigin(origins = "http://localhost:8081")
@RestController
@RequestMapping("/api")
public class LoginController {

	@Autowired
	private UserRepository userRepository;
	
	@PostMapping("/login")
	public ResponseEntity<String> login(@RequestBody User user) {
		
		User dataUserValid;
		boolean isTruePassword = false;
		
		// validasi data kosong
		if (user.getUsername().trim().equalsIgnoreCase("") || user.getPassword().trim().equalsIgnoreCase("") ) {
			return new ResponseEntity<>(
			          "Username dan / atau password kosong", 
			          HttpStatus.BAD_REQUEST);
		} else {
			dataUserValid = userRepository.findByUsername(user.getUsername());
			if (dataUserValid != null ) {
				// cek password
				if (user.getPassword().equals(dataUserValid.getPassword())) {
					isTruePassword = true;
				}
			}
			
			// data tidak ada di db atau password salah
			if (dataUserValid == null || !isTruePassword) {
				return new ResponseEntity<>(
				          "", 
				          HttpStatus.UNAUTHORIZED);
			}
			
		}
		
		return new ResponseEntity<>(
		          "Sukses Login", 
		          HttpStatus.OK);
		
		
	}
	
}
